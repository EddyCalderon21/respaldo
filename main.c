#include "utilByte.h"
int main(){
        teststruct n;
        n.a = 0xffffffff;
        n.p = 0xbbae;
        n.w = "z";
        n.msg = "hola";
        int a = 7899;
        char p = 'b';
        short q = 788;
        long s = 8900000000;
        float w = 4.5f;
        double m = 17.6;
        printf("Tipo de dato: int\n");
        verBytes(&a,sizeof(a));
        printf("Tipo de dato: char\n");
        verBytes(&p,sizeof(p));
        printf("Tipo de dato: short\n");
        verBytes(&q,sizeof(q));
        printf("Tipo de dato: long\n");
        verBytes(&s,sizeof(s));
        printf("Tipo de dato: float\n");
        verBytes(&w,sizeof(w));
        printf("Tipo de dato: double\n");
        verBytes(&m,sizeof(m));
		printf("Tipo de dato: int\n");
		verBytes(&n.a,sizeof(n.a));
        printf("Tipo de dato: short\n");
        verBytes(&n.p,sizeof(n.p));
        printf("Tipo de dato: char\n");
        verBytes(&n.w,sizeof(n.w));
        printf("Tipo de dato: String\n");
        verBytes(&n.msg,sizeof(n.msg));
        return 0;
}

